CREATE DATABASE GGktech;

USE GGktech;
 
CREATE TABLE Persons(P_Id INT PRIMARY KEY,LastName VARCHAR(15) NOT NULL,FirstName VARCHAR(15) NOT NULL,
Address VARCHAR(50) NOT NULL,City VARCHAR(15) NOT NULL);

CREATE TABLE Orders(O_Id INT PRIMARY KEY,OrderNo INT NOT NULL,P_Id INT NOT NULL);

INSERT INTO Persons
VALUES 
(1,'Hansen','Ola','Timoteivn 10','Sandnes'),
(2,'Svendson','Tove','Borgvn 23','Sandnes'),
(3,'Pettersen','Kari','Storgt 20','Stavanger');

INSERT INTO Orders
VALUES 
(1,77895,3),
(2,44678,3),
(3,22456,1),
(4,24562,1),
(5,34764,15);

SELECT Persons.FirstName,Persons.LastName,Orders.OrderNo
FROM Persons
INNER JOIN Orders ON
Persons.P_Id=Orders.P_Id;

SELECT FirstName,LastName,Address,City,O_Id,OrderNo
FROM Persons
LEFT JOIN Orders ON Persons.P_Id= Orders.P_Id;

SELECT * FROM Persons
LEFT JOIN Orders ON Persons.P_Id = Orders.P_Id
UNION
SELECT * FROM Persons
RIGHT JOIN Orders ON Persons.P_Id = Orders.P_Id;
