﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class GenericStack<T>
    {
        int capacity;
        T[] stack;
        int top;

        public GenericStack(int MaxElements)
        {
            capacity = MaxElements;
            stack = new T[capacity];
            top = -1;
        }

        public int Push(T Element)
        {
            //Check Overflow
            if (top == capacity - 1)
            {
                return -1;
            }
            else
            {
                // insert elementt into stack
                top = top + 1;
                stack[top] = Element;
            }
            return 0;
        }

        public T Pop()
        {
            T RemovedElement;
            T temp = default(T);
            //check Underflow
            if (!(top <= 0))
            {
                RemovedElement = stack[top];
                top = top - 1;
                return RemovedElement;
            }
            return temp;
        }

        public T Search(int position)
        {
            T temp = default(T);
            //check if Position is Valid or not
            if (position < capacity && position >= 0)
            {
                return stack[position];
            }
            return temp;
        }

        public T[] GetAllStackElements()
        {
            T[] Elements = new T[top + 1];
            Array.Copy(stack, 0, Elements, 0, top + 1);
            return Elements;
        }
    }
        class Program
        {
            static void Main(string[] args)
            {
                int capacity;
                Console.WriteLine("Enter Capacity of Stack :");
                capacity = int.Parse(Console.ReadLine());

                GenericStack<string> stack = new GenericStack<string>(10);

                while (true)
                {

                    Console.WriteLine("----------------------");
                    Console.WriteLine("\tMENU");
                    Console.WriteLine("----------------------");
                    Console.WriteLine("\t1.Push");
                    Console.WriteLine("\t2.Pop");
                    Console.WriteLine("\t3.Search");
                    Console.WriteLine("\t4.Print Full Stack");
                    Console.WriteLine("\t5.Exit");

                    Console.WriteLine("Eneter your Choice :");

                    int choice = int.Parse(Console.ReadLine());

                    switch (choice)
                    {

                        case 1:
                            {
                                Console.WriteLine("Enter String to Push :");
                                string temp = Console.ReadLine();

                                int result = stack.Push(temp);

                                if (result != -1)
                                {
                                    Console.WriteLine("Element Pushed into Stack !");
                                }
                                else
                                {
                                    Console.WriteLine("Stack Overflow !");
                                }

                                break;
                            }

                        case 2:
                            {

                                string Result = stack.Pop();

                                if (Result != null)
                                {
                                    Console.WriteLine("Deleted Element :" + Result);
                                }
                                else
                                {
                                    Console.WriteLine("Stack Underflow !");
                                }

                                break;
                            }

                        case 3:
                            {

                                Console.WriteLine("Enter Position of Element to Pop:");

                                int Position = int.Parse(Console.ReadLine());

                                string Result = stack.Search(Position);

                                if (Result != null)
                                {
                                    Console.WriteLine("Element at Position" + Position + " is " + Result);
                                }
                                else
                                {
                                    Console.WriteLine("Entered Element is Out of Stack Range ");
                                }

                                break;
                            }

                        case 4:
                            {

                                string[] Elements = stack.GetAllStackElements();

                                Console.WriteLine("---------------- Stack Content ---------------");

                                foreach (string str in Elements)
                                {
                                    Console.WriteLine(str);
                                }

                                break;
                            }

                        case 5:
                            {

                                System.Diagnostics.Process.GetCurrentProcess().Kill();

                                break;
                            }

                        default:
                            {
                                Console.WriteLine("Entered Wrong Choice ");
                                break;
                            }

                    }

                }

            }

        }
    }

