﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{

    class BST<T> where T : IComparable
    {
        Node<T> root = null;
        int _size = 0;
        public int size
        {
            get
            {
                return _size;
            }
        }
        public void Insert(T data)
        {
            _size++;
            if (root == null)
            {
                root = new Node<T>(data);
                return;
            }
            Node<T> current = root;
            while (true)
            {
                if (data.CompareTo(current.data) < 0)
                {
                    if (current.left == null)
                    {
                        current.left = new Node<T>(data);
                        return;
                    }
                    current = current.left;
                }
                else
                {
                    if (current.right == null)
                    {
                        current.right = new Node<T>(data);
                        return;
                    }
                    current = current.right;
                }
            }
        }
        public void PrintInOrder()
        {
            PrintInOrder(root);
        }
        void PrintInOrder(Node<T> root)
        {
            if (root == null) return;
            PrintInOrder(root.left);
            Console.Write(root.data + " ");
            PrintInOrder(root.right);
        }



    }
    class Node<T> where T : IComparable
    {
        T _data;
        Node<T> _left;
        Node<T> _right;
        public Node(T data)
        {
            this.data = data;
        }
        public int CompareTo(object obj)
        {
            return this.CompareTo(obj);
        }
        public T data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }
        public Node<T> left
        {
            get
            {
                return _left;
            }
            set
            {
                _left = value;
            }
        }
        public Node<T> right
        {
            get
            {
                return _right;
            }
            set
            {
                _right = value;
            }
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            BST<int> bst = new BST<int>();
            Console.WriteLine("Enter no. of nodes in tree:");
            int node = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter values");
            for (int i = 0; i < node; i++)
            {
                int value= Convert.ToInt32(Console.ReadLine());
                bst.Insert(value);
            }
            bst.PrintInOrder();
        }
    }
}
