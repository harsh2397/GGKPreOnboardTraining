﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    class Bus : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Bus is building");
        }
    }

}
