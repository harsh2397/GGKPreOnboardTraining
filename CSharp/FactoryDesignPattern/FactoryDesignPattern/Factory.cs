﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDesignPattern
{
    class Factory
    {
        public IVehicle BuildVehicle(String vehicleType)
        {
            if (vehicleType == null)
            {
                return null;
            }
            if (vehicleType.Equals("bus",StringComparison.InvariantCultureIgnoreCase))
            {
                return new Bus();

            }
            if (vehicleType.Equals("car", StringComparison.InvariantCultureIgnoreCase))
            {
                return new Car();

            }
            if (vehicleType.Equals("truck", StringComparison.InvariantCultureIgnoreCase))
            {
                return new Truck();
            }

            return null;
        }

        static void Main(string[] args)
        {
            Factory factory = new Factory();
            IVehicle bus = factory.BuildVehicle("bus");
            bus.Build();
            IVehicle car = factory.BuildVehicle("car");
            car.Build();
            IVehicle truck = factory.BuildVehicle("truck");
            truck.Build();
            Console.ReadKey();
        }
    }
}
