﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class AsciiConversion
    {

        public static Boolean IsPrime(int j)
        {

            int flag = 0;
            for (int i = 2; i <= Math.Sqrt(j); i++)
            {
                if (j % i == 0)
                {
                    flag++;
                    break;
                }
            }
            if (flag == 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        static void Main(string[] args)
        {
            int length;
            int[] ascii;
            char[] output;
            String input = String.Empty;

            input = Console.ReadLine();
            length = input.Length;

            ascii = new int[length - 1];
            output = new char[length - 1];

            for (int i = 0; i < length - 1; i++)
            {
                ascii[i] = (input[i] + input[i + 1]) / 2;

                Boolean t = IsPrime(ascii[i]);

                if (t == true)
                {
                    ascii[i]++;
                }

                Console.Write((char)ascii[i]);

            }

        }
    }
}
