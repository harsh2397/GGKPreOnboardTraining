﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class ConsecutivePrimeSum
    {
        public static Boolean IsPrime(int j)
        {

            int flag = 0;
            for (int i = 2; i <= Math.Sqrt(j); i++)
            {
                if (j % i == 0)
                {
                    flag++;
                    break;
                }
            }
            if (flag == 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static int NextPrime(int j)
        {
            Boolean T;
            for (int i = j + 1; ; i++)
            {
                T = IsPrime(i);
                if (T == true)
                {
                    return i;
                }

            }
        }
        static void Main(string[] args)
        {
            int input, result = 0, sum = 0;
            Boolean prime;

            input = Convert.ToInt32(Console.ReadLine());

            for (int i = 3; i <= input; i++)
            {
                prime = IsPrime(i);
                sum = 0;
                if (prime == false)
                {

                }
                else
                {
                    int z;
                    for (int j = 2; j < i; j = z)
                    {
                        sum = sum + j;
                        if (sum == i)
                        {
                            result++;
                            break;
                        }
                        z = NextPrime(j);
                    }
                }

            }

            Console.WriteLine("Output :" + result);
        }
    }
}

