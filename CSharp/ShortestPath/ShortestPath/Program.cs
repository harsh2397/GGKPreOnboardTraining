﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GGK
{
    class Vertex
    {
        public string name;
        public int status;
        public int precedor;
        public int pathLength;
    }
    class ShortestPath
    {
        private readonly int NIL = -1;
        private readonly int TEMPORARY = 1;
        private readonly int PERMANENT = 2;
        private readonly int INFINITY = 999;

        Vertex[] vertexList;
        int[,] adjacent;
        int size, currentVertex;
        ShortestPath(int size)
        {
            adjacent = new int[size, size];
            vertexList = new Vertex[size];
            this.size = size;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    adjacent[i, j] = INFINITY;
                }
            }
        }

        void NameVertex(int index, string name)
        {
            vertexList[index] = new Vertex();
            vertexList[index].name = name;
        }

        int GetIndexByName(string name)
        {
            int index = NIL;
            for (int i = 0; i < size; i++)
            {
                if (vertexList[i].name == name)
                {
                    index = i;
                }
            }
            return index;
        }

        void InsertPathLength(string vertex1Name, string vertex2Name, int pathLength)
        {
            int vertex1 = GetIndexByName(vertex1Name);
            int vertex2 = GetIndexByName(vertex2Name);

            adjacent[vertex1, vertex2] = pathLength;
            adjacent[vertex2, vertex1] = pathLength;
        }

        int TemporaryMinPathLengthVertex()
        {
            int min = INFINITY;
            int vertex = NIL;

            for (int i = 0; i < size; i++)
            {
                if (vertexList[i].status == TEMPORARY && vertexList[i].pathLength < min)
                {
                    min = vertexList[i].pathLength;
                    vertex = i;
                }

            }
            return vertex;
        }

        bool IsAdjacent(int vertex1, int vertex2)
        {
            if (adjacent[vertex1, vertex2] != INFINITY)
                return true;
            else
                return false;
        }

        void Route(int source)
        {
            for (int i = 0; i < size; i++)
            {
                vertexList[i].status = TEMPORARY;
                vertexList[i].pathLength = INFINITY;
                vertexList[i].precedor = NIL;
            }

            vertexList[source].pathLength = 0;

            while (true)
            {
                currentVertex = TemporaryMinPathLengthVertex();

                if (currentVertex == NIL)
                    return;

                vertexList[currentVertex].status = PERMANENT;

                for (int i = 0; i < size; i++)
                {
                    if (IsAdjacent(currentVertex, i) == true && vertexList[i].status == TEMPORARY)
                    {
                        if (vertexList[currentVertex].pathLength + adjacent[currentVertex, i] < vertexList[i].pathLength)
                        {
                            vertexList[i].precedor = currentVertex;
                            vertexList[i].pathLength = vertexList[currentVertex].pathLength + adjacent[currentVertex, i];
                        }
                    }
                }

            }
        }

        void FindPath(int source, int destination)
        {
            int[] path = new int[size];
            int count = 0, shortestDistance = 0, adjPredecessor;
            Console.Write("Shortest Path from {0} to {1} is :\n", vertexList[source].name, vertexList[destination].name);

            while (destination != source)
            {
                path[count++] = destination;
                adjPredecessor = vertexList[destination].precedor;
                shortestDistance += adjacent[destination, adjPredecessor];
                destination = adjPredecessor;
            }
            path[count++] = source;

            for (int i = count - 1; i > 0; i--)
            {
                Console.Write(vertexList[path[i]].name + "-->");
            }
            Console.WriteLine(vertexList[path[0]].name + " with total distance of " + shortestDistance + "\n");
            shortestDistance = 0;
        }

        static void Main(string[] args)
        {
            ShortestPath shortestPath = new ShortestPath(10);
            char choice;
            string[] input;
            string source, destination;
            int noOfEdges, i;

            //naming as per the assignment question.
            shortestPath.NameVertex(0, "1");
            shortestPath.NameVertex(1, "2");
            shortestPath.NameVertex(2, "3");
            shortestPath.NameVertex(3, "4");
            shortestPath.NameVertex(4, "5");
            shortestPath.NameVertex(5, "6");
            shortestPath.NameVertex(6, "9");
            shortestPath.NameVertex(7, "12");
            shortestPath.NameVertex(8, "10");
            shortestPath.NameVertex(9, "13");

            //inserting path length
            shortestPath.InsertPathLength("1", "2", 1);
            shortestPath.InsertPathLength("1", "3", 2);
            shortestPath.InsertPathLength("2", "5", 3);
            shortestPath.InsertPathLength("3", "4", 8);
            shortestPath.InsertPathLength("5", "6", 1);
            shortestPath.InsertPathLength("4", "6", 2);
            shortestPath.InsertPathLength("5", "9", 4);
            shortestPath.InsertPathLength("4", "12", 8);
            shortestPath.InsertPathLength("9", "10", 1);
            shortestPath.InsertPathLength("12", "13", 2);
            shortestPath.InsertPathLength("10", "13", 3);


            //Displaying the hard-coded input
            Console.WriteLine("Example\nTotal number of vertices : 10\n" +
                "Adjacent vertices followed by path length : ");
            Console.WriteLine(
                "1 2 1\n" +
                "1 3 2\n" +
                "2 5 3\n" +
                "3 4 8\n" +
                "5 6 1\n" +
                "4 6 2\n" +
                "5 9 4\n" +
                "4 12 8\n" +
                "9 10 1\n" +
                "12 13 2\n" +
                "10 13 3\n" +
                "Souce:1\nDestination:13\n");

            shortestPath.Route(0);
            shortestPath.FindPath(0, 9);

            Console.WriteLine("Do you want to find shortest distance for any other graph ? ( Y / n ) : ");
            choice = char.Parse(Console.ReadLine());
            if (choice == 'Y')
            {
                Console.WriteLine("Enter the total number of vertices : ");
                int noOfVertices = int.Parse(Console.ReadLine());

                shortestPath = new ShortestPath(noOfVertices);

                Console.WriteLine("Enter the name of all the vertex. Example A B C");
                input = Console.ReadLine().Split( );

                for (i = 0; i < noOfVertices; i++)
                {
                    shortestPath.NameVertex(i, input[i]);
                }

                Console.WriteLine("Enter the number of edges in the graph : ");
                noOfEdges = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter the path length. Example  if there is a path from vertex 1 to 2 having pathlength 4\nEnter 1 2 4");

                for (i = 0; i < noOfEdges; i++)
                {
                    input = (Console.ReadLine()).Split(' ');
                    shortestPath.InsertPathLength(input[0].Trim(), input[1].Trim(), int.Parse(input[2].Trim()));
                }

                Console.Write("Enter source vertex : ");
                source = Console.ReadLine();
                Console.Write("Enter destination vertex : ");
                destination = Console.ReadLine();

                shortestPath.Route(shortestPath.GetIndexByName(source));
                shortestPath.FindPath(shortestPath.GetIndexByName(source), shortestPath.GetIndexByName(destination));

                Console.ReadKey();
            }
        }
    }
}
