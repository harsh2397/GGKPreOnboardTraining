﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{

    class Node
    {
        public int item;
        public Node left;
        public Node right;
    }

    class Bst
    {
        public Node root;
        public Bst()
        {
            root = null;
        }

        public void Insert(int value)
        {
            Node newNode = new Node();
            newNode.item = value;
            if (root == null)
            {
                root = newNode;
            }
            else
            {
                Node current = root;
                Node parent;

                while (true)
                {
                    parent = current;
                    if (value < current.item)
                    {
                        current = current.left;
                        if (current == null)
                        {
                            parent.left = newNode;
                            return;
                        }
                    }
                    else
                    {
                        current = current.right;
                        if (current == null)
                        {
                            parent.right = newNode;
                            return;
                        }
                    }

                }
            }
        }
        public void Ascending(Node Root)
        {
            if (Root != null)
            {
                Ascending(Root.left);
                Console.Write(Root.item + " ");
                Ascending(Root.right);
            }
        }
        public void Descending(Node Root)
        {
            if (Root != null)
            {
                Descending(Root.right);
                Console.Write(Root.item + " ");
                Descending(Root.left);
            }
        }
        public Boolean IsNodePresent(int data,Node root)

        {
            Node current;
            current = root;

            if (current == null)
            {
                return false;
            }
            if (data == current.item)
            {
                return true;
            }
            if (data < current.item)
            {
                return IsNodePresent(data, current.left);
            }
            else
            {
                return IsNodePresent(data, current.right);
            }
        }
    }
    class BinarySearchTree
    {
        static void Main(string[] args)
        {
            int input;

            Bst Tree = new Bst();

            Console.WriteLine("Enter total no. of nodes:");

            input = Convert.ToInt32(Console.ReadLine());
            
            //Enter data of nodes
            for(int i = 0; i < input; i++)
            {
                int element = Convert.ToInt32(Console.ReadLine());

                Tree.Insert(element);
            }

            /* Tree.Insert(50);
            Tree.Insert(17);
            Tree.Insert(23);
            Tree.Insert(12);
            Tree.Insert(19);
            Tree.Insert(54);
            Tree.Insert(9);
            Tree.Insert(14);
            Tree.Insert(67);
            Tree.Insert(76);
            Tree.Insert(72);
            */

            Console.WriteLine("Ascending Order: ");
            Tree.Ascending(Tree.root);

            Console.WriteLine("\nDesscending Order: ");
            Tree.Descending(Tree.root);
            

            Console.WriteLine("\nEnter Element to search :");
            int data=Convert.ToInt32(Console.ReadLine());


            Boolean result = Tree.IsNodePresent(data, Tree.root);

            Console.WriteLine(result);

            if (result==true)
            {
                Console.WriteLine("Element Found");
            }
            else
            {
                Console.WriteLine("Element not found");
            }
        }
    }
    
}
