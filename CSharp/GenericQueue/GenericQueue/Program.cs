﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class Node<T>
    {
        T element;
        Node<T> nxt;
        public Node(T element)
        {
            this.element = element;
        }
        public T data
        {
            get
            {
                return element;
            }
            set
            {
                element = value;
            }
        }
        public Node<T> next
        {
            get
            {
                return nxt;
            }
            set
            {
                nxt = value;
            }
        }

    }

    class Queue<T>
    {
        Node<T> front = null;
        Node<T> rear = null;
        int qsize = 0;
        public void EnQueue(T data)
        {
            if (front == null)
            {
                front = new Node<T>(data);
                rear = front;
            }
            else
            {
                Node<T> temp = new Node<T>(data);
                front.next = temp;
                front = temp;
            }
            qsize++;
        }
        public T DeQueue()
        {
            if (rear == null)
            {
                throw new Exception("empty queue");
            }
            Node<T> temp = rear;
            rear = rear.next;
            temp.next = null;
            qsize--;
            return temp.data;
        }
        public int size
        {
            get
            {
                return qsize;
            }
        }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder("[]");
            Node<T> curr = rear.next;
            str.Insert(1, rear.data + ",");
            while (curr != null)
            {
                str.Insert(str.Length - 1, curr.data + ",");
                curr = curr.next;
            }
            str.Remove(str.Length - 2, 1);
            return str.ToString();
        }
    }





    class Program
    {
        static void Main(string[] args)
        {
            Queue<String> queue = new Queue<String>();

            while (true)
            {

                Console.WriteLine("----------------------");
                Console.WriteLine("\tMENU");
                Console.WriteLine("----------------------");
                Console.WriteLine("\t1.Enqueue");
                Console.WriteLine("\t2.Dequeue");
                Console.WriteLine("\t3.Size of Queue");
                Console.WriteLine("\t4.Print Full Queue");
                Console.WriteLine("\t5.Exit");

                Console.WriteLine("Eneter your Choice :");

                int choice = int.Parse(Console.ReadLine());

                switch (choice)
                {

                    case 1:
                        {
                            Console.WriteLine("Enter String to Push :");
                            string temp = Console.ReadLine();
                            queue.EnQueue(temp);
                            break;
                        }

                    case 2:
                        {
                            Console.WriteLine("Removed = " + queue.DeQueue());
                            break;
                        }

                    case 3:
                        {
                            Console.WriteLine("size = " + queue.size);
                            break;
                        }

                    case 4:
                        {

                            Console.WriteLine("---------------- Stack Content ---------------");
                            Console.WriteLine(queue);
                            break;
                        }

                    case 5:
                        {

                            System.Diagnostics.Process.GetCurrentProcess().Kill();

                            break;
                        }

                    default:
                        {
                            Console.WriteLine("Entered Wrong Choice ");
                            break;
                        }

                }

            }
        }
    }
}
