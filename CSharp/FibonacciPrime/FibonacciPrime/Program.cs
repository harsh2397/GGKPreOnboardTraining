﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class FibonacciPrime
    {

        public static Boolean IsPrime(uint number)
        {

            int flag = 0;
            for (int i = 2; i <= Math.Sqrt(number); i++)
            {
                if (number % i == 0)
                {
                    flag++;
                    break;
                }
            }
            if (flag == 1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        static void Main(string[] args)
        {
            int input;
            uint first = 1, second = 1, tempresult;
            String result = String.Empty;

            input = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= input-1; i++)
            {
                tempresult = first + second;
                first = second;
                second = tempresult;

                Boolean Res = IsPrime(tempresult);

                if (Res == true)
                {
                    result = result + "\t" + tempresult;
                    Console.WriteLine(result);
                }
            }
            Console.Read();
        }
    }
}
