﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GGK
{
    class AddingNumbers
    {

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
        static void Main(string[] args)
        {
            int length1, length2,carry = 0,add;

            String input1 = String.Empty;
            String input2 = String.Empty;
            String result = String.Empty;
           

            input1 = Console.ReadLine();
            input2 = Console.ReadLine();

            length1 = input1.Length;
            length2 = input2.Length;
            
            // keeping always larger string in input1
            if (length2 > length1)
            {
                String temp = input1;
                input1 = input2;
                input2 = temp;
            }

            input1 = ReverseString(input1);
            input2 = ReverseString(input2);
          
            for (int i = 0; i < length2; i++)
            {
                
                add = Convert.ToInt32(""+input1[i]) + Convert.ToInt32(""+input2[i]) + carry;

                carry = add / 10;
                add = add % 10;
                
                result = add + result ;    
            }

            for (int i = length2; i < length1; i++)
            {

                add = Convert.ToInt32("" + input1[i]) + carry;

                carry = add / 10;
                add = add % 10;

                result = add + result;
            }
            if(carry!=0)
            {
                result = carry + result;
            }
            Console.WriteLine("ADDITION IS :"+result);

        }
    }
}
